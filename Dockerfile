FROM debian:bookworm-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./recollwebui /app

# Install any needed packages specified in requirements.txt
# RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Install recoll
RUN apt-get update && apt-get install -y recoll
RUN apt-get install -y --no-install-recommends python3 python3-pip git python3-waitress

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Define environment variable
# ENV NAME World

# Run app.py when the container launches
# RUN python3 webui-standalone.py

# Run webui-standalone.py.py when the container launches
CMD ["/usr/bin/python3", "/app/webui-standalone.py", "-a", "0.0.0.0"]